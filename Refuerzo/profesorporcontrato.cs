﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExamenMendozaJheison
{
    class profesorporcontrato:profesor,ICalcular
    {
        private int SueldoBasico = 500;
        public int ValorHoraEx { get; set; }

        public void CalculoCobrar()
        {
            int resultado;
            resultado = SueldoBasico + ValorHoraEx;
            Console.WriteLine("Sueldo a recibir: $" + resultado);
        }
        public override void Mostrar()
        {
            Console.WriteLine("*****Profesor por Contrato*****");
            base.Mostrar();
        }
    }
}
