﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExamenMendozaJheison
{
    class profesorporhora:profesor, ICalcular
    {
        public double PrecioPorHora { get; set; }
        public double HorasTrabajadas { get; set; }


        public void CalculoCobrar()
        {
            PrecioPorHora = 20;
            HorasTrabajadas = 2;
            double resultado;
            resultado = HorasTrabajadas * PrecioPorHora;
            Console.WriteLine("El valor a pagar es: $" + resultado);
        }
        public override void Mostrar()
        {
            Console.WriteLine("*****Profesor por Hora*****");
            base.Mostrar();
        }
    }
}
