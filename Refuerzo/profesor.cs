﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExamenMendozaJheison
{
    public class profesor
    {
        //Propiedades de las Caracteristicas de las entidades
        public string Nombres { get; set; }
        public string Apellidos { get; set; }
        public string Dirección { get; set; }
        public int Cedulaidentidad { get; set; }

        // Definir un método que permita mostrar por pantalla todos los datos de las propiedades de la entidad
        public virtual void Mostrar()
        {
            Console.WriteLine("Nombres: "+ Nombres);
            Console.WriteLine("Apellidos: "+Apellidos);
            Console.WriteLine("Dirección: "+Dirección);
            Console.WriteLine("CedulaIdentidad: "+Cedulaidentidad);
        }

    }
}
