﻿using System;
using System.Collections.Generic;

namespace ExamenMendozaJheison
{
    class Program
    {
        static void Main(string[] args)
        {
            profesornombramiento profesor1 = new profesornombramiento();
            profesor1.Nombres = "Juan";
            profesor1.Apellidos = "Rodriguez";
            profesor1.Dirección = "Manta";
            profesor1.Cedulaidentidad = 1306469879;

            profesorporcontrato profesor2 = new profesorporcontrato();
            profesor2.Nombres = "Jheison";
            profesor2.Apellidos = "Zambrano";
            profesor2.Dirección = "Chone";
            profesor2.Cedulaidentidad = 1309984652;


            profesorporhora profesor3 = new profesorporhora();
            profesor3.Nombres = "Paul";
            profesor3.Apellidos = "Gonzales";
            profesor3.Dirección = "Manta";
            profesor3.Cedulaidentidad = 1308879542;

            List<profesor> listaprofesores = new List<profesor>();
            listaprofesores.Add(profesor1);
            listaprofesores.Add(profesor2);
            listaprofesores.Add(profesor3);
            
            foreach (ICalcular profesor in listaprofesores)
            {
                profesor.Mostrar();
                profesor.CalculoCobrar();
                Console.WriteLine();
            
            }
            
        }
    }
}
